use std::mem::{transmute,transmute_copy};
use std::collections::HashMap;

pub enum Layer {
	Vec(Vec<Value>),
	Map(HashMap<String, Value>),
}

#[derive(Debug)]
pub enum Value {
	I64(i64),
	I32(i32),
	I16(i16),
	I8(i8),
	U64(u64),
	U32(u32),
	U16(u16),
	U8(u8),
	String(String),
	Map(HashMap<String, Value>),
	Vec(Vec<Value>),
}

#[derive(PartialEq,Eq)]
pub enum ValType {
	I64,
	I32,
	I16,
	I8,
	U64,
	U32,
	U16,
	U8,
	String,
	Vec,
	Map,
}

impl ValType {
	pub fn as_num(&self) -> u8 {
		match self {
			&ValType::I64 => 0,
			&ValType::I32 => 1,
			&ValType::I16 => 2,
			&ValType::I8 => 3,
			&ValType::U64 => 4,
			&ValType::U32 => 5,
			&ValType::U16 => 6,
			&ValType::U8 => 7,
			&ValType::String => 8,
			&ValType::Vec => 9,
			&ValType::Map => 10,
		}
	}
	
	pub fn from_num(num: &u8) -> Option<ValType> {
		Some(match num {
			&0 => ValType::I64,
			&1 => ValType::I32,
			&2 => ValType::I16,
			&3 => ValType::I8,
			&4 => ValType::U64,
			&5 => ValType::U32,
			&6 => ValType::U16,
			&7 => ValType::U8,
			&8 => ValType::String,
			&9 => ValType::Vec,
			&10 => ValType::Map,
			_ => return None
		})
	}
	
	pub fn byte_size(&self) -> usize {
		match self {
			&ValType::I64 => 8,
			&ValType::I32 => 4,
			&ValType::I16 => 2,
			&ValType::I8 => 1,
			&ValType::U64 => 8,
			&ValType::U32 => 4,
			&ValType::U16 => 2,
			&ValType::U8 => 1,
			_ => 0
		}
	}
}

impl Value {
	pub fn val_type(&self) -> ValType {
		match self {
			&Value::I64(_) => ValType::I64,
			&Value::I32(_) => ValType::I32,
			&Value::I16(_) => ValType::I16,
			&Value::I8(_) => ValType::I8,
			&Value::U64(_) => ValType::U64,
			&Value::U32(_) => ValType::U32,
			&Value::U16(_) => ValType::U16,
			&Value::U8(_) => ValType::U8,
			&Value::String(_) => ValType::String,
			&Value::Vec(_) => ValType::Vec,
			&Value::Map(_) => ValType::Map,
		}
	}
	
	pub fn raw<T: Valuable>(&self) -> Option<&T> {
		T::inner(self)
	}
	
	pub fn raw_mut<T: Valuable>(&mut self) -> Option<&mut T> {
		T::inner_mut(self)
	}

	pub fn map_ref(&self) -> Option<&HashMap<String, Value>> {
		match self {
			&Value::Map(ref map) => Some(map),
			_ => None
		}
	}
	
	pub fn map_ref_mut(&mut self) -> Option<&mut HashMap<String, Value>> {
		match self {
			&mut Value::Map(ref mut map) => Some(map),
			_ => None
		}
	}

	pub fn bytes(&self) -> Vec<u8> {
		unsafe {
			match self {
				&Value::I64(ref val) => {
					let array: [u8; 8] = transmute_copy(val);
					array.to_vec()
				}, &Value::I32(ref val) => {
					let array: [u8; 4] = transmute_copy(val);
					array.to_vec()
				}, &Value::I16(ref val) => {
					let array: [u8; 2] = transmute_copy(val);
					array.to_vec()
				}, &Value::I8(ref val) => {
					let array: [u8; 1] = transmute_copy(val);
					array.to_vec()
				}, &Value::U64(ref val) => {
					let array: [u8; 8] = transmute_copy(val);
					array.to_vec()
				}, &Value::U32(ref val) => {
					let array: [u8; 4] = transmute_copy(val);
					array.to_vec()
				}, &Value::U16(ref val) => {
					let array: [u8; 2] = transmute_copy(val);
					array.to_vec()
				}, &Value::U8(ref val) => {
					let array: [u8; 1] = transmute_copy(val);
					array.to_vec()
				}, &Value::String(ref val) => val.clone().into_bytes(),
				&Value::Vec(_) => {
					Vec::new()
				}, &Value::Map(_) => {
					Vec::new()
				},
			}
		}
	}
	
	pub fn from_bytes(t: &ValType, bytes: Vec<u8>) -> Option<Value> {
		if t.byte_size() != 0 && bytes.len() != t.byte_size() {
			return None;
		} match t {
			&ValType::I64 => {
				let array = [
					bytes[0], bytes[1], bytes[2], bytes[3],
					bytes[4], bytes[5], bytes[6], bytes[7]
				]; let val: i64 = unsafe { transmute(array) };
				Some(Value::I64(val))
			}, &ValType::U64 => {
				let array = [
					bytes[0], bytes[1], bytes[2], bytes[3],
					bytes[4], bytes[5], bytes[6], bytes[7]
				]; let val: u64 = unsafe { transmute(array) };
				Some(Value::U64(val))
			}, &ValType::I32 => {
				let array = [
					bytes[0], bytes[1], bytes[2], bytes[3],
				]; let val: i32 = unsafe { transmute(array) };
				Some(Value::I32(val))
			}, &ValType::U32 => {
				let array = [
					bytes[0], bytes[1], bytes[2], bytes[3],
				]; let val: u32 = unsafe { transmute(array) };
				Some(Value::U32(val))
			}, &ValType::I16 => {
				let array = [
					bytes[0], bytes[1]
				]; let val: i16 = unsafe { transmute(array) };
				Some(Value::I16(val))
			}, &ValType::U16 => {
				let array = [
					bytes[0], bytes[1]
				]; let val: u16 = unsafe { transmute(array) };
				Some(Value::U16(val))
			}, &ValType::I8 => {
				let array = [
					bytes[0]
				]; let val: i8 = unsafe { transmute(array) };
				Some(Value::I8(val))
			}, &ValType::U8 => {
				let array = [
					bytes[0]
				]; let val: u8 = unsafe { transmute(array) };
				Some(Value::U8(val))
			}, &ValType::String => Some(Value::String(String::from_utf8_lossy(bytes.as_slice()).into_owned())),
			_ => None
		}
	}
}

pub trait Valuable: Sized {
	fn into_value(self) -> Value;
	fn val_type(&self) -> ValType;
	fn from_value(val: Value) -> Option<Self>;
	fn inner(val: &Value) -> Option<&Self>;
	fn inner_mut(val: &mut Value) -> Option<&mut Self>;
}

impl Valuable for Value {
	fn into_value(self) -> Value {
		self
	} fn val_type(&self) -> ValType {
		self.val_type()
	} fn from_value(val: Value) -> Option<Self> {
		Some(val)
	} fn inner(val: &Value) -> Option<&Self> {
		Some(val)
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		Some(val)
	}
} impl Valuable for i64 {
	fn into_value(self) -> Value {
		Value::I64(self)
	} fn val_type(&self) -> ValType {
		ValType::I64
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::I64(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::I64(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::I64(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for i32 {
	fn into_value(self) -> Value {
		Value::I32(self)
	} fn val_type(&self) -> ValType {
		ValType::I32
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::I32(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::I32(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::I32(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for i16 {
	fn into_value(self) -> Value {
		Value::I16(self)
	} fn val_type(&self) -> ValType {
		ValType::I16
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::I16(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::I16(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::I16(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for i8 {
	fn into_value(self) -> Value {
		Value::I8(self)
	} fn val_type(&self) -> ValType {
		ValType::I8
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::I8(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::I8(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::I8(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for u64 {
	fn into_value(self) -> Value {
		Value::U64(self)
	} fn val_type(&self) -> ValType {
		ValType::U64
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::U64(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::U64(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::U64(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for u32 {
	fn into_value(self) -> Value {
		Value::U32(self)
	} fn val_type(&self) -> ValType {
		ValType::U32
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::U32(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::U32(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::U32(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for u16 {
	fn into_value(self) -> Value {
		Value::U16(self)
	} fn val_type(&self) -> ValType {
		ValType::U16
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::U16(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::U16(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::U16(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for u8 {
	fn into_value(self) -> Value {
		Value::U8(self)
	} fn val_type(&self) -> ValType {
		ValType::U8
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::U8(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::U8(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::U8(ref mut v) => Some(v),
			_ => None
		}
	}
} impl Valuable for String {
	fn into_value(self) -> Value {
		Value::String(self)
	} fn val_type(&self) -> ValType {
		ValType::String
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::String(v) => Some(v),
			_ => None
		}
	} fn inner(val: &Value) -> Option<&Self> {
		match val {
			&Value::String(ref v) => Some(v),
			_ => None
		}
	} fn inner_mut(val: &mut Value) -> Option<&mut Self> {
		match val {
			&mut Value::String(ref mut v) => Some(v),
			_ => None
		}
	}
} impl<V: Valuable> Valuable for HashMap<String, V> {
	fn into_value(self) -> Value {
		let mut new = HashMap::new();
		for (key, val) in self {
			new.insert(key, val.into_value());
		} Value::Map(new)
	} fn val_type(&self) -> ValType {
		ValType::Map
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::Map(map) => {
				let mut new = HashMap::new();
				for (key, val) in map {
					if let Some(some) = V::from_value(val) {
						new.insert(key, some);
					}
				} Some(new)
			}, _ => None
		}
	} fn inner(_: &Value) -> Option<&Self> {
		None	
	} fn inner_mut(_: &mut Value) -> Option<&mut Self> {
		None
	}
} impl<V: Valuable> Valuable for Vec<V> {
	fn into_value(self) -> Value {
		let mut new = Vec::new();
		for val in self {
			new.push(val.into_value());
		} Value::Vec(new)
	} fn val_type(&self) -> ValType {
		ValType::Vec
	} fn from_value(val: Value) -> Option<Self> {
		match val {
			Value::Vec(vec) => {
				let mut new = Vec::new();
				for val in vec {
					if let Some(some) = V::from_value(val) {
						new.push(some);
					}
				} Some(new)
			}, _ => None
		}
	} fn inner(_: &Value) -> Option<&Self> {
		None
	} fn inner_mut(_: &mut Value) -> Option<&mut Self> {
		None
	}
}
