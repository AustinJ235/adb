pub mod types;
use std::fs;
use std::io::{self,Seek,Write,Read};

#[test]
fn test() {
	use Adb;
	
	let mut db = Adb::new();
	db.set(&["Math", "1+2"], 3_i16).unwrap();
	db.set(&["Math", "3+6"], 9_i8).unwrap();
	db.set(&["Math", "3/0"], String::from("Undefined"));
	db.set(&["Math", "George", "is_bob"], 1_u8);
	db.print();
	db.save(format!("db"));
	
	let mut db = Adb::load(format!("db")).unwrap();
	db.set(&["Math", "1+2"], 4_i8).unwrap();
	db.save(format!("db"));
	
	let mut db = Adb::load(format!("db")).unwrap();
	db.print();
}

use std::collections::HashMap;
use types::*;

struct Meta {
	pub written: bool,
	pub up_data: bool,
	pub up_next: bool,
	pub location: u64,
	pub len: u64,
	pub next: u64,
	pub data: u64,
	pub typ: u8,
	pub dlen: u64,
}

struct Dead {
	pub location: u64,
	pub length: u64,
}

pub struct Adb {
	map: HashMap<String, Value>,
	dead_space: Vec<Dead>,
	file_len: u64,
	metas: HashMap<Vec<String>, Meta>,
}

impl Adb {
	pub fn new() -> Self {
		Adb {
			map: HashMap::new(),
			dead_space: Vec::new(),
			metas: HashMap::new(),
			file_len: 8, // first 8 bytes reserved for first meta offset
		}
	}
	
	pub fn load(path: String) -> Result<Self, String> {
		let mut metas = HashMap::new();
		let mut handle = match fs::OpenOptions::new()
				.read(true)
				.open(path.as_str()) {
			Ok(ok) => ok,
			Err(e) => return Err(format!("IO Error attempting to open file, {}", e))
		}; let file_len = match handle.metadata() {
			Ok(ok) => {
				let mut len = ok.len();
				if len < 9 {
					len = 8;
				} len
			}, Err(e) => return Err(format!("IO Error attempting to read metadata, {}", e))
		};
		
		// Get the offset of the first meta
		let mut first_meta_buf = [0;8];	
		if let Err(e) = handle.read_exact(&mut first_meta_buf) {
			return Err(format!("IO Error attempting to read first 8 bytes, {}", e))
		} let mut cur_off = u64::from_value(Value::from_bytes(&ValType::U64, first_meta_buf.to_vec()).unwrap()).unwrap();
		
		loop {
			if cur_off == 0_u64 {
				break;
			} if let Err(e) = handle.seek(io::SeekFrom::Start(cur_off)) {
				return Err(format!("IO Error attempting to seek to {}, {}", cur_off, e));
			}
			
			let mut len_buf = [0;8];
			
			if let Err(e) = handle.read_exact(&mut len_buf) {
				return Err(format!("IO Error attempting to read key length at offset {}, {}", cur_off, e));
			}
			
			let len = u64::from_value(Value::from_bytes(&ValType::U64, len_buf.to_vec()).unwrap()).unwrap();
			let mut rest_of_buf = Vec::new();
			rest_of_buf.resize(len as usize + 25_usize, 0);
			
			if let Err(e) = handle.read_exact(rest_of_buf.as_mut_slice()) {
				return Err(format!("IO Error attempting to read rest of meta entry at offset {}, {}", cur_off, e))
			}
			
			let keys_bytes = rest_of_buf.split_off(25);
			let dlen_bytes = rest_of_buf.split_off(17);
			let typ = rest_of_buf.pop().unwrap();
			let data_bytes = rest_of_buf.split_off(8);
			let next_bytes = rest_of_buf;
			let dlen = u64::from_value(Value::from_bytes(&ValType::U64, dlen_bytes).unwrap()).unwrap();
			let data = u64::from_value(Value::from_bytes(&ValType::U64, data_bytes).unwrap()).unwrap();
			let next = u64::from_value(Value::from_bytes(&ValType::U64, next_bytes).unwrap()).unwrap();
			let mut keys_split = Vec::new();
			keys_split.push(Vec::new());
			let mut keys = Vec::new();
			
			for byte in keys_bytes {
				if byte == 2 {
					keys_split.push(Vec::new());
				} else {
					keys_split.last_mut().unwrap().push(byte);
				}
			} for key_bytes in keys_split {
				let key = match String::from_utf8(key_bytes) {
					Ok(ok) => ok,
					Err(e) => return Err(format!("Invalid UTF-8 for key at offset {}, {}", cur_off, e))
				}; keys.push(key);
			}
			
			if cfg!(test) {
				println!("Adb::load() - Loaded Meta :: Key Length: {}, Next Offset: {}, Data Offset: {}, Data Type: {}, Data Length: {}, Key: {:?}", len, next, data, typ, dlen, keys);
			}

			metas.insert(keys, Meta {
				written: true,
				up_data: false,
				up_next: false,
				location: cur_off.clone(),
				len: len,
				next: next,
				data: data,
				typ: typ,
				dlen: dlen
			}); cur_off = next.clone();
		}

		let mut to_set = Vec::new();
		
		for (keys, meta) in &metas {
			if let Err(e) = handle.seek(io::SeekFrom::Start(meta.data)) {
				return Err(format!("IO Error attempting to seek to {} reading data for key {:?}, {}", meta.data, keys, e));
			}
			
			let mut buf = Vec::new();
			buf.resize(meta.dlen as usize, 0);
			
			if let Err(e) = handle.read_exact(buf.as_mut_slice()) {
				return Err(format!("IO Error attempting to read data at {} for key {:?}, {}", meta.data, keys, e))
			}
			
			let val_type = match ValType::from_num(&meta.typ) {
				Some(some) => some,
				None => return Err(format!("Key {:?} contains an invalid type byte.", keys))
			}; let value = match Value::from_bytes(&val_type, buf) {
				Some(some) => some,
				None => return Err(format!("Key {:?} has incompatible data for type specified.", keys))
			}; to_set.push((keys.clone(), value));
			
		}
		
		let mut adb = Adb {
			map: HashMap::new(),
			dead_space: Vec::new(),
			metas: metas,
			file_len: file_len,
		};
		
		for (keys, value) in to_set {
			let key_string = format!("{:?}", keys);
			if let Err(e) = adb.load_value(keys, value) {
				return Err(format!("Failed to load value for key {}, {}", key_string, e));
			}
		}
		
		Ok(adb)
	}
	
	pub fn save(&mut self, path: String) -> Result<(), String> {
		// Convert infinitly deep map into a 1d vec
		let data_1d = self.data_1d();
		let mut todo = Vec::new();
		
		// Loop through each value
		for (keys, typ, bytes) in data_1d {
			// Attempt to get the meta for this key
			if let Some(meta) = self.metas.get_mut(&keys) {
				// if the data has been written and an update has happened the old space
				// may have to be removed
				if meta.written && meta.up_data {
					// If the data length has changed mark the old location as dead space
					// and set the data to be rewritten
					if meta.dlen != bytes.len() as u64 {
						self.dead_space.push(Dead {
							location: meta.data.clone(),
							length: meta.dlen.clone(),
						}); meta.data = 0;
					}
				} if meta.up_data || !meta.written {
					todo.push((keys, bytes));
				} continue;
			}
			
			// Key length is defined by key vec len + len of each key
			let mut key_len = keys.len()-1_usize;
			for key in &keys {
				key_len += key.len();
			}
		
			// meta not found so create the meta
			self.metas.insert(keys.clone(), Meta {
				written: false,
				up_data: true,
				up_next: false,
				location: 0,
				len: key_len as u64,
				next: 0,
				data: 0,
				typ: typ,
				dlen: bytes.len() as u64,
			}); todo.push((keys, bytes));
		}
		
		// Create vec to contain bytes to write
		let mut to_write = Vec::new();
		to_write.resize(8, None);
		let mut file_len = self.file_len.clone();
		
		// where to write the offset for the current meta location
		let mut last_meta = None;
		let mut write_begin = None;
		let mut write_next = Vec::new();
		
		for (keys, meta) in &mut self.metas {
			if meta.written && meta.next == 0 {
				let mut in_todo = false;
				
				for &(ref todo_keys, _) in &todo {
					if *keys == *todo_keys {
						in_todo = true;
						last_meta = Some(keys.clone());
						break;
					}
				} if !in_todo {
					todo.push((keys.clone(), Vec::new()));
					last_meta = Some(keys.clone());
				}
			}
		}
		
		// Go through each todo item and find space for it to be written to
		for &(ref keys, _) in &todo {
			// Get meta. Should exist if it is in todo; otherwise, continue to avoid panic
			let mut meta = match self.metas.get_mut(keys) {
				Some(some) => some,
				None => continue
			};
			
			// If the meta isn't written, find somewhere to write it and update the next offset
			if !meta.written {
				// calc the space required for this meta
				let sp_req = meta.len + 33_u64;
				let mut found_dead = false;
				
				// attempt to find some dead space that will fit this meta entry
				for dead in &mut self.dead_space {
					if dead.length >= sp_req {
						// found space update the dead and use this space for this meta entry
						dead.length -= sp_req;
						meta.location = dead.location.clone();
						dead.location += sp_req;
						found_dead = true;
						break;
					}
				}
				
				// didn't find any dead space so append to the end of the file
				if !found_dead {
					meta.location = file_len as u64;
					file_len += sp_req;
				}
				
				// Set the offset to the last_meta and replace last_meta with current
				match last_meta.take() {
					Some(some) => write_next.push((some, meta.location.clone())),
					None => write_begin = Some(meta.location.clone())
				} last_meta = Some(keys.clone());
			}
			
			// data isn't written so find a space for it to be written
			if meta.data == 0 {
				let mut found_dead = false;
				
				// attempt to find some dead space that will fit this data entry
				for dead in &mut self.dead_space {
					if dead.length >= meta.dlen {
						// found some space, update the dead and use this space for the data entry
						dead.length -= meta.dlen;
						meta.data = dead.location.clone();
						dead.location += meta.dlen;
						found_dead = true;
						break;
					}
				}
				
				if !found_dead {
					meta.data = file_len as u64;
					file_len += meta.dlen;
				}
			}
		}
		
		// Up the the next values on metas
		for (keys, value) in write_next {
			let mut meta = match self.metas.get_mut(&keys) {
				Some(some) => some,
				None => continue
			};
			
			meta.next = value;
			meta.up_next = true;
		}
		
		// Write the first bytes if it was set
		if let Some(some) = write_begin {
			let bytes = Value::U64(some).bytes();
			for i in 0..8 {
				to_write[i] = Some(bytes[i]);
			}
		}
		
		// Now that all the locations are found, write the data write into the vec
		for (keys, bytes) in todo {
			// get the meta, should exist. Continue otherwise to avoid panic.
			let meta = match self.metas.get(&keys) {
				Some(some) => some,
				None => continue
			};
			
			// make sure to_write is big enough for meta
			let sp_req = meta.location as usize + meta.len as usize + 33_usize;
			if to_write.len() < sp_req {
				to_write.resize(sp_req, None);
			}
			
			// if it hasn't been written before everything needs to be written
			if !meta.written {
				// Convert meta values into bytes
				let len_bytes = Value::U64(meta.len.clone()).bytes();
				let next_bytes = Value::U64(meta.next.clone()).bytes();
				let data_bytes = Value::U64(meta.data.clone()).bytes();
				let type_byte = meta.typ.clone();
				let dlen_bytes = Value::U64(meta.dlen.clone()).bytes();
				let mut key_bytes = Vec::new();
				
				for key in keys {
					key_bytes.append(&mut key.into_bytes());
					key_bytes.push(2_u8);
				} key_bytes.pop();
				
				// Write u64 values
				let off = meta.location as usize;
				
				for i in 0..8 {
					to_write[off + i] = Some(len_bytes[i]);
					to_write[off + 8_usize + i] = Some(next_bytes[i]);
					to_write[off + 16_usize + i] = Some(data_bytes[i]);
					to_write[off + 25_usize + i] = Some(dlen_bytes[i]);
				}
				
				// Write type byte
				to_write[off + 24] = Some(type_byte);
				
				// Write the key bytes
				for i in 0..key_bytes.len() {
					to_write[off + 33 + i] = Some(key_bytes[i]);
				}
			} else {
				// meta already has been written so only update the needed values
				
				// update the data related values
				if meta.up_data {
					// Convert data related values to bytes
					let data_bytes = Value::U64(meta.data.clone()).bytes();
					let type_byte = meta.typ.clone();
					let dlen_bytes = Value::U64(meta.dlen.clone()).bytes();
					
					// Write u64 values
					let off = meta.location as usize;
					for i in 0..8 {
						to_write[off + 16_usize + i] = Some(data_bytes[i]);
						to_write[off + 25_usize + i] = Some(dlen_bytes[i]);
					}
					
					// Write type byte
					to_write[off + 24] = Some(type_byte);
				}
				
				// update next
				if meta.up_next {
					// convert next value to bytes
					let next_bytes = Value::U64(meta.next.clone()).bytes();
					
					// Write u64 values
					let off = meta.location as usize;
					for i in 0..8 {
						to_write[off + 8_usize + i] = Some(next_bytes[i]);
					}
				}
			}
			
			// if data update is required write the data bytes
			if meta.up_data {
				// make sure to_write is big enough for data
				let sp_req = meta.data as usize + meta.dlen as usize;
				if to_write.len() < sp_req {
					to_write.resize(sp_req, None);
				}
				
				// write data bytes
				for i in 0..bytes.len() {
					to_write[meta.data as usize + i] = Some(bytes[i]);
				}
			}
		}
		
		// Convert to_write into chunks to be written
		let mut chunks = Vec::new();
		let mut cur_chunk = Vec::new();
		let mut cur_chunk_pos = 0_usize;
		let mut pos = 0_usize;
		
		if cfg!(test) {
			let mut display = Vec::new();
		
			for byte in &to_write {
				if let &Some(ref some) = byte {
					if *some > 31 && *some < 127 {
						display.push(format!("{}", String::from_utf8(vec![some.clone()]).unwrap()));
					} else {
						display.push(format!("{}", some));
					}
				} else {
					display.push(format!("---"));
				}
			}
			
			println!("");
			
			for byte in display {
				print!("{:03}|", byte);
			}
			
			println!("");
		}
	
		for byte_ in to_write {
			if let Some(byte) = byte_ {
				if cur_chunk.is_empty() {
					cur_chunk_pos = pos;
				} cur_chunk.push(byte);
			} else {
				if cur_chunk.len() > 0 {
					chunks.push((cur_chunk_pos, cur_chunk.split_off(0)));
				}
			} pos += 1;
		}
	
		if !cur_chunk.is_empty() {
			chunks.push((cur_chunk_pos, cur_chunk.split_off(0)));
		}
	
		// Write the chunks to file
		{
			let mut handle = match fs::OpenOptions::new()
					.write(true)
					.create(true)
					.open(path.as_str()) {
				Ok(ok) => ok,
				Err(e) => return Err(format!("{}", e))
			};
	
			for (offset, bytes) in chunks {
				if let Err(e) = handle.seek(io::SeekFrom::Start(offset as u64)) {
					return Err(format!("{}", e));
				} if let Err(e) = handle.write_all(bytes.as_slice()) {
					return Err(format!("{}", e));
				}
			}
		}
		
		// Remove dead that have a length of zero
		let mut remove_dead = Vec::new();
		for i in 0..self.dead_space.len() {
			if self.dead_space[i].length == 0 {
				remove_dead.push(i);
			}
		} for i in remove_dead.into_iter().rev() {
			self.dead_space.remove(i);
		}
		
		// set all meta to written and clear any marked updates
		for (_, meta) in &mut self.metas {
			meta.up_next = false;
			meta.up_data = false;
			meta.written = true;
		}
		
		Ok(())
	}
	
	fn sub_data_1d(key_vec: Vec<String>, val: &Value) -> Vec<(Vec<String>, u8, Vec<u8>)> {
		let mut out = Vec::new();
		match val {
			&Value::Map(ref map) => {
				for (key, val) in map {
					let mut sub_key_vec = key_vec.clone();
					sub_key_vec.push(key.clone());
					out.append(&mut Self::sub_data_1d(sub_key_vec, val));
				}
			}, &Value::Vec(_) => {
				()
			}, _ => {
				out.push((key_vec, val.val_type().as_num(), val.bytes()));
			}
		} out
	}
	
	fn data_1d(&self) -> Vec<(Vec<String>, u8, Vec<u8>)> {
		let mut all = Vec::new();
		
		for (key, val) in &self.map {
			let mut key_vec = Vec::new();
			key_vec.push(key.clone());
			all.append(&mut Self::sub_data_1d(key_vec, val));
		} all
	}
	
	/// Set the value for this db.
	///   - Will create maps for keys that are not found.
	pub fn set<T: Valuable, K: AsRef<str>>(&mut self, key_arr: &[K], val: T) -> Result<(), String> {
		unsafe {
			// Convert key array of str's to a vec of strings
			let mut key = Vec::new();
			
			for k_item in key_arr {
				key.push(String::from(k_item.as_ref()));
			}
			
			if key.len() == 0 {
				return Err(format!("Key length must be at least one!"))
			}
			
			let meta_key = key.clone();
			let meta_typ;
			let meta_dlen;
		
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *mut HashMap<String, Value> = &mut self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be inserted into the current map
				if iter.peek().is_none() {
					let ref mut map = *cur_map;
					let val_type = val.val_type();
					meta_typ = val_type.as_num();
					meta_dlen = val_type.byte_size() as u64;
					map.insert(key, val.into_value());
					break;
				}
				
				// Attempt to get the map with this key and set current map
				{ 
					let ref mut map = *cur_map;
					if let Some(some) = map.get_mut(&key) {
						cur_map = match some.map_ref_mut() {
							Some(some) => some,
							None => return Err(format!("Value isn't a map!"))
						}; continue;
					}
				}
			
				// Create the map with this key and set the current map to it
				let ref mut map = *cur_map;
				map.insert(key.clone(), Value::Map(HashMap::new()));
				cur_map = map.get_mut(&key).unwrap().map_ref_mut().unwrap();
			}
			
			if let Some(some) = self.metas.get_mut(&meta_key) {
				some.up_data = true;
				some.dlen = meta_dlen;
				some.typ = meta_typ;
			} Ok(())
		}
	}
	
	/// Used when loading values from file. Does the same thing as set() but will not
	/// cause it to be updated and takes a raw value not something that implements valuable.
	pub fn load_value(&mut self, key: Vec<String>, val: Value) -> Result<(), String> {
		unsafe {
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *mut HashMap<String, Value> = &mut self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be inserted into the current map
				if iter.peek().is_none() {
					let ref mut map = *cur_map;
					map.insert(key, val);
					break;
				}
				
				// Attempt to get the map with this key and set current map
				{ 
					let ref mut map = *cur_map;
					if let Some(some) = map.get_mut(&key) {
						cur_map = match some.map_ref_mut() {
							Some(some) => some,
							None => return Err(format!("Value isn't a map!"))
						}; continue;
					}
				}
			
				// Create the map with this key and set the current map to it
				let ref mut map = *cur_map;
				map.insert(key.clone(), Value::Map(HashMap::new()));
				cur_map = map.get_mut(&key).unwrap().map_ref_mut().unwrap();
			} Ok(())
		}
	}
	
	/// Get a reference to a set value.
	///   - Maps will return None, use get_map()
	///   - Vecs will return None, use get_vec()
	pub fn get<T: Valuable, K: AsRef<str>>(&mut self, key_arr: &[K]) -> Option<&T> {
		unsafe {
			// Convert key array of str to a vec of strings
			let mut key = Vec::new();
			
			for k_item in key_arr {
				key.push(String::from(k_item.as_ref()));
			}
		
			if key.len() == 0 {
				return None;
			}
		
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *const HashMap<String, Value> = &self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be got from the current map
				if iter.peek().is_none() {
					let ref map = *cur_map;
					return match map.get(&key) {
						Some(some) => T::inner(some),
						None => None
					};
				}
				
				// Attempt to get the map with this key and set current map
				let ref map = *cur_map;
				if let Some(some) = map.get(&key) {
					cur_map = match some.map_ref() {
						Some(some) => some,
						None => return None
					}; continue;
				} return None;
			}
		}
	}
	
	pub fn get_map<K: AsRef<str>>(&self, key_arr: &[K]) -> Option<&HashMap<String, Value>> {
		unsafe {
			// Convert key array of str to a vec of strings
			let mut key = Vec::new();
			
			for k_item in key_arr {
				key.push(String::from(k_item.as_ref()));
			}
		
			if key.len() == 0 {
				return None;
			}
		
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *const HashMap<String, Value> = &self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be got from the current map
				if iter.peek().is_none() {
					let ref map = *cur_map;
					return match map.get(&key) {
						Some(some) => match some {
							&Value::Map(ref map) => Some(map),
							_ => None
						}, None => None
					};
				}
				
				// Attempt to get the map with this key and set current map
				let ref map = *cur_map;
				if let Some(some) = map.get(&key) {
					cur_map = match some.map_ref() {
						Some(some) => some,
						None => return None
					}; continue;
				} return None;
			}
		}
	}
	
	/// Get a mutable reference to a set value.
	///   - Maps will return None, use get_mut_map()
	///   - Vecs will return None, use get_mut_vec()
	pub fn get_mut<T: Valuable, K: AsRef<str>>(&mut self, key_arr: &[K]) -> Option<&mut T> {
		unsafe {
			// Convert key array of str to a vec of strings
			let mut key = Vec::new();
			
			for k_item in key_arr {
				key.push(String::from(k_item.as_ref()));
			}
		
			if key.len() == 0 {
				return None;
			}
		
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *mut HashMap<String, Value> = &mut self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be got from the current map
				if iter.peek().is_none() {
					let ref mut map = *cur_map;
					return match map.get_mut(&key) {
						Some(some) => T::inner_mut(some),
						None => None
					};
				}
				
				// Attempt to get the map with this key and set current map
				let ref mut map = *cur_map;
				if let Some(some) = map.get_mut(&key) {
					cur_map = match some.map_ref_mut() {
						Some(some) => some,
						None => return None
					}; continue;
				} return None;
			}
		}
	}
	
	pub fn get_mut_map<K: AsRef<str>>(&mut self, key_arr: &[K]) -> Option<&mut HashMap<String, Value>> {
		unsafe {
			// Convert key array of str to a vec of strings
			let mut key = Vec::new();
			
			for k_item in key_arr {
				key.push(String::from(k_item.as_ref()));
			}
		
			if key.len() == 0 {
				return None;
			}
		
			// Convert key vec into a iter and set the current map
			let mut iter = key.into_iter().peekable();
			let mut cur_map: *mut HashMap<String, Value> = &mut self.map;
		
			loop {
				let key = iter.next().unwrap();
				
				// This is the last key, so it should be got from the current map
				if iter.peek().is_none() {
					let ref mut map = *cur_map;
					return match map.get_mut(&key) {
						Some(some) => match some {
							&mut Value::Map(ref mut map) => Some(map),
							_ => None
						}, None => None
					};
				}
				
				// Attempt to get the map with this key and set current map
				let ref mut map = *cur_map;
				if let Some(some) = map.get_mut(&key) {
					cur_map = match some.map_ref_mut() {
						Some(some) => some,
						None => return None
					}; continue;
				} return None;
			}
		}
	}
	
	pub fn print(&self) {
		println!("{:?}", self.map);
	}
}

